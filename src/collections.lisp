(in-package :senzill.collections)

(defmacro push-back (item place)
  "Item is appended to the (right) end of the place (list)"
  `(setf ,place (append ,place (list ,item))))

(defmacro pop-back (place)
  "Removes destructively last element and returns it"
  `(let ((val (car (last ,place))))
     (setq ,place (nbutlast ,place))
     val))

(defun slice (place &key (begin 0) (end (length place)))
  "Returns list of elements from begin to end indecies, inclusive"
  (butlast (nthcdr begin place) (- (length place) end 1)))
