(in-package :cl)

(defpackage senzill.math
  (:use :cl)
  (:export :%
           :+=
           :++1
           :-=
           :--
           ://=
           :*=
           :^
           :avg)
  (:documentation "Math functions"))

(defpackage senzill.collections
  (:use :cl)
  (:export :push-back
           :pop-back
           :slice)
  (:documentation "List functions"))

(defpackage senzill.io
  (:use :cl)
  (:export :doread-lines
           :ask-for-stream)
  (:documentation "I/O functions"))
