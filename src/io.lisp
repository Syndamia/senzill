(in-package :senzill.io)

(defmacro doread-lines ((var &key (ends '("end")) read-line-options) &rest body)
  "Reads lines until NIL or input is inside ends. Each line is stored in var."
  `(let ((,var (read-line ,@read-line-options)))
     (loop until (or (member ,var ',ends :test 'string=) (not ,var)) do
           ,@body
           (setf ,var (read-line ,@read-line-options)))))

(defmacro ask-for-stream ((var) &rest body)
  "var is either *standard-input* or file stream, where the file stream is handled by with-open-stream"
  `(let ((filename))
     (format t "File name (nothing for *standard-input*): ")
     (force-output)
     (setf filename (read-line))
     (if (string= filename "")
      (let ((,var *standard-input*))
           ,@body)
      (with-open-stream (,var (open filename)) ,@body))))
