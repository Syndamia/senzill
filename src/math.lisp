(in-package senzill.math)

(defmacro % (number divisor)
  "Different name for the mod function"
  `(mod ,number ,divisor))

(defmacro op= (operation value &rest others)
  "Executes operation on all values and stores result in first argument"
  `(setf ,value (apply ,operation ,value ',others)))

(defmacro += (value &rest numbers)
  "Sums all numbers and stores result in name (first argument)"
  `(op= '+ ,value ,@numbers))

(defmacro ++1 (value)
  "Adds increments value by one"
  `(+= ,value 1))

(defmacro -= (value &rest numbers)
  "Adds all numbers and stores result in name (first argument)"
  `(op= '- ,value ,@numbers))

(defmacro -- (value)
  "Decrements value by one"
  `(-= ,value 1))

(defmacro //= (value &rest numbers)
  "Divides all numbers and stores result in name (first argument)"
  `(op= '/ ,value ,@numbers))

(defmacro *= (value &rest numbers)
  "Multiplies all numbers and stores result in name (first argument)"
  `(op= '* ,value ,@numbers))

(defun ^ (number power)
  "Calculates number to the power"
  (let ((prod 1))
    (dotimes (x power)
      (setq prod (* prod number)))
    prod))

(defun avg (&rest numbers)
  "Calculates the average of all given numbers. If no numbers are given, returns 0."
  (if (not numbers)
    0
    (/ (apply '+ numbers) (length numbers))))
