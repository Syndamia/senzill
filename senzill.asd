(defsystem senzill
  :author "Kamen Mladenov <kamen@syndamia.com>"
  :maintainer "Kamen Mladenov <kamen@syndamia.com>"
  :license "GNU GPLv3"
  :version "0.2"
  :homepage "https://gitlab.com/Syndamia/senzill"
  :bug-tracker "https://gitlab.com/Syndamia/senzill/-/issues"
  :source-control (:git "git@gitlab.com:Syndamia/senzill.git")
  :components ((:module "src"
                :components
                ((:file "packages")
                 (:file "math" :depends-on ("packages"))
                 (:file "collections" :depends-on ("packages"))
                 (:file "io" :depends-on ("packages"))))))
